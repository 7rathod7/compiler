#!/usr/bin/python2

import sys
import ply.lex as lex
import ply.yacc as yacc
import util

# counts for static,pointer and assignemnt statements
count_static=0
count_pointer=0
count_assign=0

# file_name
file_name=''

# debug
debug = 0

ifelsecounter=0

# for storing declaration and assignment statements in method
node_list = []

# list for arguments
arg_list = []

# param list
par_list = []

# return node
ret_node = ''

# procedure table
proc_table = []

# variable table
scope = 0
function_scope = 0
var_table = []
# this variable stores identifiers
dec_list = []

# this variable stores declaration of function
temp_dec_list = []

# for storing the methods
nest_node_list = []

cond_list = []
cond_list2 = []

check_ = 1
count_bb=1

# tokens are declared here
tokens = (
		'NAME',
		# 'MAIN',
		'NUMBER',
		'RETURN',
		'FLOAT',
		'INT',
		# 'NEWLINE',
		'VOID',
		'COMMA',
		'PLUS',
		'MINUS',
		'DIVIDE',
		'SEMICOLON',
		'PERCENTAGE',
		'LPAREN',
		'RPAREN',
		'EQUALS',
		'LTHAN',
		'GTHAN',
		'LBRACE',
		'RBRACE',
		# 'UMINUS',
		'EXCLAIM',
		'ORLINE',
		'IF',
		# 'IFX',
		'ELSE',
		'WHILE',
		# 'LBRACKET',
		# 'RBRACKET',
		'ASTERISK',
		'AMPERSAND',
)



#definitions of tokens
t_ASTERISK = r'\*'
t_NUMBER = r'[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?'
# t_FLOAT = r'[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?'
t_AMPERSAND = r'&'
t_LTHAN = r'<'
t_EXCLAIM = r'!'
t_GTHAN = r'>'
# t_NAME = r'[a-zA-Z_][a-zA-Z0-9_]*'
t_COMMA = r','
t_ORLINE = r'\|'
t_PLUS = r'\+'
t_MINUS = r'-'
t_DIVIDE = r'/'
t_LBRACE = r'{'
t_RBRACE = r'}'
t_SEMICOLON = r';'
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_EQUALS = r'='
t_PERCENTAGE = r'%'
# t_LBRACKET = r'\['
# t_RBRACKET = r'\]'

lineno = 1


def t_NAME(t):
	r'[a-zA-Z_][a-zA-Z0-9_]*'
	if (t.value == 'if'):
		t.type = 'IF'
	if (t.value == 'else'):
		t.type = 'ELSE'
	if (t.value == 'while'):
		t.type = 'WHILE'
	# if (t.value == 'main'):
	# 	t.type = 'MAIN'
	if (t.value == 'return'):
		t.type = 'RETURN'
	if (t.value == 'void'):
		t.type = 'VOID'
	if (t.value == 'int'):
		t.type = 'INT'
	if (t.value == 'float'):
		t.type = 'FLOAT'
	return t

def t_NEWLINE(t):
	r'\n'
	global lineno
	lineno+=1
#    return t

t_ignore = " \t"

temp=''

def t_error(t):
	print "Line %d." % (t.lineno,) + "",
	if t.value[0] == '"':
		print "Unterminated string literal."
		if t.value.count('\n') > 0:
			t.skip(t.value.index('\n'))
	elif t.value[0:2] == '/*':
		print "Unterminated comment."
	else:
		print "Illegal character '%s'" % t.value[0]
		t.skip(1)

# precedence rules are declared here
precedence = (
		('left','PLUS','MINUS'),
		('left','ASTERISK','DIVIDE'),
		('right', 'UMINUS'),
		('nonassoc','IFX'),
		('nonassoc','ELSE'),   
)

# argument should be of form 'func/number'
def get_func_name(y):
	i=0
	for x in y:
		if x=='/':
			break
		else:
			i = i + 1
	return y[0:i]

def p_cprog(p):
	'''
	cprog : sproglist
	'''
	global nest_node_list, proc_table, var_table, temp, file_name
	# print nest_node_list

	# checking for main function
	# main = 0
	# for node in list(nest_node_list):
	# 	# print node.value
	# 	if node.value == 'main':
	# 		main=1
	# 		break
	# if main == 0:
	# 	print "no main function declared"
	# 	sys.exit()

	# printing ast file
	temp = sys.stdout
	sys.stdout = open(file_name+'.c.ast','w')
	# print nest_node_list
	for node in list(nest_node_list):
		print "Function", node.value
		print "PARAMS("+get_param(node.arg)+")"
		print "RETURNS", node.rtype
		for child in node.list:
			child.printTree(1)
		if node.isreturn:
			if (len(node.ret_obj.list)!=0):
				node.ret_obj.printTree(0)
			else:
				node.ret_obj.printTree(0)
				print '(\n)'
		print ""
	sys.stdout = temp

	# cfg root
	cfgroot=0
	# printinf cfg file
	temp = sys.stdout
	sys.stdout = open(file_name+'.cfg','w')
	# print len(list(nest_node_list))
	for node in list(nest_node_list):
		print "function "+ node.value + "(" +get_param(node.arg)+")"
		# print "RETURNS", node.rtype
		if node.isreturn:
			if (len(node.ret_obj.list)!=0):
				node.list.append(node.ret_obj)
			else:
				node.list.append(node.ret_obj)
		# print node.list[0].printTree(0)
		# print "hello"
		endval,cfgroot = util.CFGPrint(util.printCFG(util.CFGTree(node.list)))

		print '<bb '+str(endval)+'>'
		print 'return'
		# for child in node.list:
		# 	child.printTree(1)
				# print '(\n)'
		print ""
	sys.stdout = temp

	# printing symbol table
	temp=sys.stdout
	sys.stdout=open(file_name+'.sym','w')
	if (debug == 1):
		print "jennie is in cprog"
	print "Procedure table :-\n-----------------------------------------------------------------"
	print "Name\t\t|\tReturn Type\t\t|\tParameter List"
	for child in proc_table:
		print child[0]+'\t\t|\t'+child[1]+'\t\t|\t'+child[2]
	print "-----------------------------------------------------------------"
	print "Variable table :- \n-----------------------------------------------------------------"
	print "Name\t|\tScope\t\t\t|\tBase Type\t|\tDerived Type\n-----------------------------------------------------------------"
	for c in var_table:
		s = util.getname(c[0],1)
		d=''
		for x in s:
			if x=='*':
				d = d+'*'
			else:
				break
		if (c[1]==0):
			print util.getname(c[0],0)+'\t|'+'procedure global\t|'+c[2]+'\t\t\t|\t'+d
		else:	
			print util.getname(c[0],0)+'\t\t|\t'+'procedure '+get_func_name(c[1])+'\t|\t'+c[2]+'\t\t\t|\t'+d
	print "-----------------------------------------------------------------"
	print "-----------------------------------------------------------------"
	sys.stdout = temp

	# code for assembly
	util.generateAssemblyCode(cfgroot, file_name)

# list of tuple like (int, node obj)
def get_param(node):
	s = ' '
	# print(len(node.list))
	if (debug == 1):
		print "jennie is in get_param"
	for child in node.list:
		# print child[1].printTree(0)
		s = s + child[0] + ' ' + util.getname(child[1],1) + " , "
	return s[1:len(s)-3]
	

def p_sproglist(p):
	'''
	sproglist : sprog
			  | sprog sproglist
	'''
	if (debug == 1):
		print "jennie is in sproglist"

def p_globaldec(p):
	'''
	sprog : stmt
	'''
	if (debug == 1):
		print "jennie is in globaldec"


def p_sprog(p):
	'''
	sprog : type expression3 arguments main_body
	'''
	funcname = util.getname(p[2],0)
	global nest_node_list, ret_node, proc_table, scope, dec_list
	global var_table, temp_dec_list, function_scope
	function_scope = 0
	scope = scope - 1
	d = util.getname(p[2],1)
	k = ''
	for i in range(0, len(d)):
		if d[i] == '*':
			k = k + '*'
		else:
			break
	alpha = 0
	for entry in proc_table:
		if entry[0] == funcname:
			alpha = 1
	if (alpha == 0 and funcname != 'main'):
			proc_table.append((funcname, p[1] + k, get_param(p[3]),p[3]))
	rev_node_list = list(reversed(p[4]))
	if (len(rev_node_list) >= 0):
		for child in temp_dec_list:
			# print child
			var_table.append((child[0], funcname + '/' +str(child[1]), child[2]))
		dec_list = []
		Node = util.Node()
		Node.arg = p[3]
		Node.rtype = k + p[1]
		Node.value = funcname
		Node.ismain=0
		Node.isreturn=0
		if (Node.value == 'main'):
			Node.ismain = 1
		for child in rev_node_list:
			Node.addChild(child)
		if ret_node != '':
			Node.isreturn = 1
			Node.ret_obj = ret_node
		nest_node_list.append(Node)
		ret_node=''
	
	dec_list = []
	temp_dec_list = []
	if (debug == 1):
		print "jennie is in sprog", scope, funcname


def p_arguments(p):
	'''
	arguments : LPAREN params RPAREN
	'''
	global par_list, function_scope, dec_list, temp_dec_list, scope
	scope = scope + 1
	function_scope = function_scope + 1
	node = util.Node()
	for child in reversed(dec_list):
		# print child
		temp_dec_list.append((child[1], function_scope, child[0]))
	for child in reversed(par_list):
		# print child[1].printTree(0)
		node.addChild(child)
	p[0]=node
	function_scope = function_scope + 1
	dec_list = []
	par_list = []
	if (debug == 1):
		print "jennie is in arguments"

def p_params(p):
	'''
	params : type expression3 COMMA params
			| type expression3
	'''
	global par_list, dec_list
	# p[2].printTree(0)
	par_list.append((p[1],p[2]))
	dec_list.append((p[1],p[2]))
	if (debug == 1):
		print "jennie is in params"

def p_params1(p):
	'''
	params : 
	'''
	if (debug == 1):
		print "jennie is in param1"

def p_main_body(p):
	'''
	main_body : LBRACE RBRACE
			  | LBRACE stmt_list RBRACE
			  | SEMICOLON
	'''
	global node_list
	p[0] = node_list
	# print node_list
	node_list=[]
	if (debug == 1):
		print "jennie is in main_body",p[0]

def p_stmt_list(p):
	'''
	stmt_list : stmt stmt_list
			  | stmt
	'''
	global check_
	global node_list
	global cond_list
	if (check_ != 1 and p[1]):
		# print check_,len(cond_list)
		cond_list[check_-2][0].append(p[1])
		p[0]=p[1]
	elif (p[1]):
		node_list.append(p[1])
		p[0]=p[1]
	# if(p[1]):
	# 	p[1].printTree(0)
	if (debug == 1):
		print "jennie is in stmt_list",p[1],check_

def p_decl_stmt(p):
	'''
	stmt : type varlist SEMICOLON
	'''
	global dec_list, scope, var_table, temp_dec_list, function_scope,check_
	# function_scope = function_scope + 1
	for x in dec_list:
		# x.printTree(0)
		if (scope == 0):
			var_table.append((x,scope,p[1]))
		else:
			temp_dec_list.append((x,function_scope+check_-1,p[1]))
	# print "======================="
	dec_list = []
	if (debug == 1):
		print "jennie in decl_stmt, scope", scope

# assignlist -> assign comma assignlist semicolon | assign semicolon
# assign -> starname equals number | starname equals starname | name equals name | name equals andname

def p_stmt(p):
	'''
	stmt : assignlist SEMICOLON
	'''
	p[0]=p[1]
	if (debug == 1):
		print "jennie is in stmt",p[1]

def p_select_stmt(p):
	'''
	stmt : selection_stmt
	'''
	p[0]=p[1]
	if (debug == 1):
		print "jennie is in select_stmt"

def p_iterate_stmt(p):
	''' 
	stmt : iteration_stmt 
	'''
	p[0]=p[1]
	if (debug == 1):
		print "jennie is in iteration_stmt"
	
def p_assignlist(p):
	'''
	assignlist : assignstmt COMMA assignlist 
				| assignstmt
	'''
	p[0]=p[1]
	if (debug == 1):
		print "jennie is in assignlist"

def p_assignstmt(p):
	'''
	assignstmt : expression3 EQUALS expression
	'''
	global count_assign,temp_dec_list, function_scope, check_
	temp = util.getname(p[1],0)
	test1 = util.loop_up_vartable(var_table, temp, temp_dec_list, function_scope+check_-1)
	test2 = util.expression_varloookup(var_table, p[3], temp_dec_list, function_scope+check_-1)
	# print temp_dec_list[0][0].printTree(0)
	if test1 == -1 or test2 == -1:
		print "variable not declared , lineno ", lineno
		sys.exit()
	node = util.Node()
	node.value = "ASGN"
	node.addChild(p[1])
	node.addChild(p[3])
	count_assign+=1
	p[0]=node
	if (debug == 1):
		print "jennie is in assignstmt->",p[3]

def p_rec_starname(p):
	'''
	rec_starname : ASTERISK rec_starname
	'''
	node = util.Node()
	node.value = "DEREF"
	node.addChild(p[2])
	p[0]=node
	if (debug == 1):
		print "jennie is in rec_starname"

def p_rec_starname1(p):
	'''
	rec_starname : starname
	'''
	p[0]=p[1]
	if (debug == 1):
		print "jennie is in p_rec_starname1"

def p_expression(p):
	'''
	expression : expression PLUS expression1
				| expression MINUS expression1
	'''
	node = util.Node()
	if p[2] == '+':
		node.value = "PLUS"
		node.addChild(p[1])
		node.addChild(p[3])
	if p[2] == '-':
		node.value = "MINUS"
		node.addChild(p[1])
		node.addChild(p[3])
	p[0]=node
	if (debug == 1):
		print "jennie is in expression"

def p_expression01(p):
	'''
	expression : expression1
	'''
	p[0]=p[1]
	if (debug == 1):
		print "jennie is in expression01"

def p_expression1(p):
	'''
	expression1 : expression1 DIVIDE unary_expr
				| expression1 ASTERISK unary_expr
				| expression1 PERCENTAGE unary_expr
	'''
	node = util.Node()
	if p[2] == '/':
		node.value = "DIV"
		node.addChild(p[1])
		node.addChild(p[3])
	if p[2] == '*':
		node.value = "MUL"
		node.addChild(p[1])
		node.addChild(p[3])
	p[0]=node
	if (debug == 1):
		print "jennie is in expression1"

def p_expression11(p):
	'''
	expression1 : unary_expr
	'''
	p[0]=p[1]
	if (debug == 1):
		print "jennie is in expression11"

def p_unary_expr(p):
	'''
	unary_expr : expression3
	'''
	p[0]=p[1]
	if (debug == 1):
		print "jennie is in unary_expr"

def p_term_expression(p):
	'''
	expression3 : rec_starname
				| andname
				| name
				| number
	'''
	p[0]=p[1]
	if (debug == 1):
		print "jennie is in p_term_expression"

def p_return_stmt(p):
	'''
	stmt : RETURN expression3 SEMICOLON
		 | RETURN SEMICOLON
	'''
	global ret_node
	node = util.Node()
	node.value = 'RETURN'
	if p[2] != ';':
		node.addChild(p[2])
	ret_node = node
	if (debug == 1):
		print "jennie is in return stmt"

def p_var_funcIni(p):
	'''
	stmt : expression3 EQUALS call
	'''
	global count_assign, temp_dec_list, function_scope, check_
	count_assign = count_assign + 1
	temp = util.getname(p[1],0)
	test1 = util.loop_up_vartable(var_table, temp, temp_dec_list, function_scope+check_-1)
	if test1 == -1:
		print "variable not declared , lineno ", lineno-1
		sys.exit()
	node = util.Node()
	node.value = 'ASGN'
	node.addChild(p[1])
	if p[3][1] == 'void':
		print "invalid expression at lineno ", lineno-1
		sys.exit()
	node.addChild(p[3][0])
	p[0]=node
	if (debug == 1):
		print "jennie is in var funcIni", p[1].printTree(0)

def p_stmt_funcall(p):
	'''
	stmt : call
	'''
	p[0]=p[1][0]
	if (debug == 1):
		print "jennie is in stmt_funcall"

def p_funCall(p):
	'''
	call : NAME LPAREN args RPAREN SEMICOLON
	'''
	global arg_list, proc_table
	test = util.loop_up_proctable(proc_table,p[1])
	if (test == -1):
		print "invalid function call at lineno", lineno-1
		sys.exit()
	param = test[0]
	returntype = test[1]
	if len(arg_list) != len(param):
		# print len(arg_list), type(param)
		print "invalid number of arguments provided at lineno", lineno-1
		sys.exit()
	node = util.Node()
	node.value = 'CALL '+p[1]
	for child in reversed(arg_list):
		# child.printTree(0)
		node.addChild(child)
	p[0]=(node, returntype)
	arg_list = []
	if (debug == 1):
		print "jennie is in funCall",p[0].printTree(0)

def p_args(p):
	'''
	args : expression3 COMMA args
		 | expression3
	'''
	global arg_list
	arg_list.append(p[1])
	if (debug == 1):
		print "jennie is in args"

def p_args1(p):
	'''
	args :
	'''
	if (debug == 1):
		print "jennie is in args1"

def p_paren_expression(p):
	'''
	expression : LPAREN expression RPAREN
	'''
	p[0]=p[2]
	if (debug == 1):
		print "jennie is in p_paren_expression"

def p_type(p):
	'''
	type : INT
		  | VOID
		  | FLOAT
	'''
	p[0]=p[1]
	if (debug == 1):
		print "jennie is in p_type"

def p_starname(p):
	'''
	starname : ASTERISK name
	'''
	node = util.Node()
	node.value = "DEREF"
	node.addChild(p[2])
	p[0] = node
	if (debug == 1):
		print "jennie is in starname"

def p_andname(p):
	'''
	andname : AMPERSAND name
	'''
	node = util.Node()
	node.value = "ADDR"
	node.addChild(p[2])
	p[0] = node
	if (debug == 1):
		print "jennie is in andname"

def p_num(p):
	'''
	number : NUMBER
	'''
	node = util.Node()
	node.value = "CONST("+str(p[1])+")"
	p[0]=node
	if (debug == 1):
		print "jennie is in num"
 
def p_name(p):
	'''
	name : NAME
	'''
	node = util.Node()
	node.value = "VAR("+p[1]+")"
	p[0]=node
	if (debug == 1):
		print "jennie is in name"

# varlist-> var COMMA varlist | var
# var -> name | starname

def p_var(p):
	'''
	var : NAME
	'''
	global count_static
	count_static+=1
	if (debug == 1):
		print "jennie is in p_var"

def p_var1(p):
	'''
	var1 : starname
	'''
	global count_pointer
	count_pointer+=1
	if (debug == 1):
		print "jennie is in var1"

def p_var2(p):
	'''
	var1 : ASTERISK var1
	'''
	if (debug == 1):
		print "jennie is in p_var2"

def p_varlist(p):
	'''
	varlist : expression3 COMMA varlist
			| expression3
	'''
	global dec_list
	dec_list.append(p[1])
	if (debug == 1):
		print "jennie is in varlist"

def p_uminus(p):
	'''
	expression : MINUS expression %prec UMINUS
	'''
	if (debug == 1):
		print "jennie is in uminus"
	node = util.Node()
	node.value = "UMINUS"
	node.addChild(p[2])
	p[0] = node
	
# selection-stmt -> if lparen cond_expr rparen stmt ;
# cond_expr -> logical-or-expr
# logical-or-expr -> logical-and-expr | logical-or || logical-and
# logical-and-expr -> inclusive-or | logical-and && inclusive-or


def p_iteration_stmt1(p):
	'''
	iteration_stmt : WHILE LPAREN condition_expression RPAREN brace_stmt
	'''
	if (debug == 1):
		print "jennie is in iteration_stmt1"
	node = util.Node()
	node.value = "WHILE"
	node.addChild(p[3])
	# node.addChild(p[5])
	global cond_list
	global check_
	for x in list(reversed(cond_list[check_-2][0])):
		node.addChild(x)
	del cond_list[check_-2]
	check_ = check_ - 1
	# if (check_ == 1):
	# 	node_list.append(node)
	p[0]=node


def p_brace_stmt(p):
	'''
	brace_stmt : stmt
	'''
	global check_, ifelsecounter
	global cond_list
	if (check_ != 1):
		# if(not ifelsecounter):
		# 	ifelsecounter+=1
		cond_list[check_-2][0].append(p[1])
	cond_list[check_-2][1].append(len(cond_list[check_-2][0]))
	if (debug == 1):
		print "jennie is in brace_stmt", "check_->",check_
	p[0]=p[1]

def p_brace_stmt1(p):
	'''
	brace_stmt : LBRACE stmt_list RBRACE
	'''
	global cond_list, ifelsecounter, check_
	if (debug == 1):
		print "jennie is in p_brace_stmt1",p[2], "check_->",check_
	if (not ifelsecounter):
		ifelsecounter =  len(cond_list[check_-2][0])
	cond_list[check_-2][1].append(len(cond_list[check_-2][0]))
	# print "testing ",ifelsecounter
	p[0]=p[2]

def p_selection_stmt(p):
	'''
	selection_stmt : IF LPAREN condition_expression RPAREN brace_stmt %prec IFX
	'''
	node = util.Node()
	node.value = "IF"
	node.addChild(p[3])
	global cond_list,ifelsecounter
	ifelsecounter = 0
	global check_
	for x in list(reversed(cond_list[check_-2][0])):
		node.addChild(x)
	del cond_list[check_-2]
	check_ = check_ - 1
	# if (check_ == 1):
	# 	node_list.append(node)
	p[0]=node
	if (debug == 1):
		print "jennie is in selection_stmt",p[3]

def p_selection_stmt1(p):
	'''
	selection_stmt : IF LPAREN condition_expression RPAREN brace_stmt ELSE brace_stmt
	'''
	node = util.Node()
	node.value = "IF"
	node.addChild(p[3])
	global cond_list
	global check_, ifelsecounter
	if (len(cond_list[check_-2][1])):
		node.elsepart = cond_list[check_-2][1][0]+1
	else:
		node.elsepart = 1
	ifpart = cond_list[check_-2][0][0:node.elsepart-1]
	# print "ifpartlist ",ifpart
	elsepart = cond_list[check_-2][0][node.elsepart-1:len(cond_list[check_-2][0])]
	# print "elsepartlist ",elsepart
	ifelsecounter = 0

	for x in reversed(ifpart):
		node.addChild(x)
	for x in reversed(elsepart):
		node.addChild(x)
	del cond_list[check_-2]
	# node.addChild(p[7])
	check_ = check_ - 1
	p[0]=node
	# node.printTree(0)
	if (debug == 1):
		print "jennie is in selection_stmt1","check_->",check_

def p_condition_expression(p):
	'''
	condition_expression : assign_expr
	'''
	global check_
	global cond_list
	cond_list.append(([],[]))
	check_ = check_ + 1
	if (debug == 1):
		print "jennie is in condition_expression"
	p[0]=p[1]

def p_assign_expr(p):
	'''
	assign_expr : log_and_expr
	'''
	if (debug == 1):
		print "jennie is in assign_expr"
	p[0]=p[1]

def p_log_and_expr(p):
	'''
	log_and_expr : log_and_expr AMPERSAND AMPERSAND eq_expr
				 | log_and_expr ORLINE ORLINE eq_expr
	'''
	if (debug == 1):
		print "jennie is in logical-and-expr"
	node = util.Node()
	if (p[2] == '&'):
		node.value = 'AND'
	else:
		node.value = 'OR'
	node.addChild(p[1])
	node.addChild(p[4])
	p[0]=node

def p_log_and_expr1(p):
	'''
	log_and_expr : eq_expr
	'''
	if (debug == 1):
		print "jennie is in p_log_and_expr1"
	p[0]=p[1]

def p_eq_expr(p):
	'''
	eq_expr : rel_expr
	'''
	if (debug == 1):
		print "jennie is in eq_expr"
	p[0]=p[1]

def p_rel_expr(p):
	'''
	rel_expr : rel_expr EXCLAIM EQUALS shift_expr
			 | rel_expr EQUALS EQUALS shift_expr
			 | rel_expr LTHAN EQUALS shift_expr
			 | rel_expr GTHAN EQUALS shift_expr
	'''
	if (debug == 1):
		print "jennie is in rel_expr",p[2]
	node = util.Node()
	if (p[2] == '<'):
		node.value="LE"
	if (p[2] == '!'):
		node.value="NE"
	if (p[2] == '>'):
		node.value="GE"
	if (p[2] == '='):
		node.value="EQ"
	node.addChild(p[1])
	node.addChild(p[4])
	p[0]=node

def p_rel_expr1(p):
	'''
	rel_expr : shift_expr
	'''
	if (debug == 1):
		print "jennie is in p_rel_expr1"
	p[0]=p[1]

def p_rel_expr2(p):
	'''
	rel_expr : rel_expr LTHAN shift_expr
			 | rel_expr GTHAN shift_expr
	'''
	if (debug == 1):
		print "jennie is in p_rel_expr2"
	node = util.Node()
	if (p[2] == '<'):
		node.value = "LT"
	if (p[2] == '>'):
		node.value = "GT"
	node.addChild(p[1])
	node.addChild(p[3])
	# node_list.append(node)
	p[0]=node

def p_shift_expr(p):
	'''
	shift_expr : expression
	'''
	if (debug == 1):
		print "jennie is in shift_expr"
	p[0]=p[1]


def p_error(t):
	# print "You've got a syntax error somewhere in your code."
	# print "It could be around line %d." % t.lineno
	# print "Good luck finding it."
	# raise ParseError()
	if t:
		print("syntax error at lineno {1} at {0}".format(t.value,lineno))
	else:
		print("syntax error at EOF")

def call_lex():
	global file_name
	file_name = sys.argv[1]
	file = open(file_name)
	lines = file.readlines()
	file.close()
	strings = ""
	for i in lines:
		strings += i
	lex.input(strings)
	# while 1:
	#     token = lex.token()       # Get a token
	#     if not token: break        # No more tokens
	#     print "(%s,'%s',%d, %d)" % (token.type, token.value, token.lineno, token.lexpos)
	yacc.yacc()
	yacc.parse(strings)
	# global count_assign
	# global count_pointer
	# global count_static
   	# print count_static
   	# print count_pointer
   	# print count_assign


lex.lex()

if __name__ == '__main__':
	call_lex()